// public views and handlers

package web

import (
    "strconv"
    "net/http"
    "codeberg.org/snaums/mvoCI/core"

    "github.com/labstack/echo/v4"
)

// /public/zip/:file
func publicZipHandler ( ctx echo.Context ) error {
    file := ctx.Param ( "file" );
    var b core.Build;
    s.db.Preload("Repository").Where("zip = ?", file).First( &b );
    if b.Repository.Public == false {
        return HTTPError ( 404, ctx )
    }
    return ctx.File ( s.cfg.Directory.Build + file );
}

// /public
// render the list of publicly visible repositories
func publicOverviewHandler ( ctx echo.Context ) error {
    if s.cfg.PublicEnable != true {
        return HTTPError ( 404, ctx );
    }
    var r []core.Repository;
    s.db.Where("public = true").Order("name asc").Find ( &r );
    return ctx.Render ( http.StatusOK, "repo_public", echo.Map{
        "navPage": "repo",
        "repositories": r,
    })
}

// /public/:id
// render the list of successful builds on a public repositoru
func publicRepoHandler ( ctx echo.Context ) error {
    if s.cfg.PublicEnable != true {
        return HTTPError ( 404, ctx );
    }
    id, _ := strconv.Atoi(ctx.Param("id"))
    pg, _ := strconv.Atoi(ctx.Param("page"))
    var page int64 = int64(pg)
    if page <= 0 {
        page = 1;
    }
    var offset int64 = (int64(page)-1)*10;
    var r core.Repository;
    s.db.Where("id = ? AND public = true", strconv.Itoa(id)).First ( &r )
    if r.ID <= 0 {
        return HTTPError ( 404, ctx );
    }

    var cnt int64;
    s.db.Model( core.Build{} ).Where("repository_id = ? AND zip != ''", r.ID).Count ( &cnt )
    var pageMax int64 = (cnt / 10) + 1
    if offset >= cnt {
        offset = (cnt / 10) * 10;
        page = (offset / 10) + 1
    }

    cnt = 0;
    var b []core.Build;
    s.db.Where("repository_id = ? AND zip != ''", r.ID).Order("finished_at DESC").Offset( int((page-1)*10) ).Limit(10).Find ( &b )

    // TODO pagination must be possible more simple...
    var pageRange []int64
    if pageMax > 9 {
        pageRange = make([]int64, 11)
        pageRange[0] = 1;
        pageRange[1] = 2;
        pageRange[2] = 3;
        if page == 4 || page == 5 {
            pageRange[3] = 4;
            pageRange[4] = 5;
            pageRange[5] = 6;
            pageRange[6] = 7;
            pageRange[7] = -1;  // => ...
        } else if page == pageMax - 4 || page == pageMax - 3 {
            pageRange[3] = -1;
            pageRange[4] = pageMax - 6;
            pageRange[5] = pageMax - 5;
            pageRange[6] = pageMax - 4;
            pageRange[7] = pageMax - 3;
        } else {
            pageRange[3] = -1;
            if page < pageMax - 4 && page > 5 {
                pageRange[4] = page - 1
                pageRange[5] = page
                pageRange[6] = page + 1
            } else {
                pageRange[4] = pageMax/2
                pageRange[5] = pageMax/2 + 1
                pageRange[6] = pageMax/2 + 2
            }
            pageRange[7] = -1
        }
        pageRange[8] = pageMax - 2;
        pageRange[9] = pageMax - 1;
        pageRange[10] = pageMax
    } else {
        pageRange = make([]int64, pageMax)
        var i int64 = 1;
        for i <= pageMax {
            pageRange[i-1] = i;
            i++;
        }
    }

    CalcBuildDuration ( b );

    return ctx.Render ( http.StatusOK, "build_public", echo.Map{
        "navPage": "repo",
        "repo": r,
        "builds": b,
        "page" : page,
        "pageMax" : pageMax,
        "pageRange": pageRange,
    })
}

