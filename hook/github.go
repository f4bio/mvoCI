// this file mostly contains the object representation of the json request send
// by the webhook feature of github

package hook

type GithubPayloadRepository struct {
    Id int
    Node_Id string
    Name string
    Full_Name string
    Private bool
    Clone_Url string
    Ssh_Url string
    Git_Url string
}

type GithubPayload struct {
    Ref string
    Before string
    After string
    Created bool
    Deleted bool
    Forced bool
    Base_Ref string
    Compare string
    Repository GithubPayloadRepository
}

